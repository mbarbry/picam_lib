from cam_lib import take_picture, get_fname 
import fractions as frac

format=['png', 'rgba']
resolution=[(640, 480), (1280, 720), (1640, 1232)]

for res in resolution:
    for f in format:
        fname=get_fname('pictures/pic_{0}_auto'.format(res[0]), f)

        take_picture(fname=fname, resolution=res, format=f, exposure_mode='auto')
