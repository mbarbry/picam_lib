import os
import sys

def get_fname(pref, format):

    fname = pref+'.'+format
    print(fname)
    if not os.path.isfile(fname):
        return fname
    else:
        i = 0
        while os.path.isfile(fname):
            fname = pref+'{0}.'.format(i)+format
            i = i + 1
        return fname


def preview(sleep=10, resolution=(640, 480), exposure_mode='auto'):
    from picamera import PiCamera
    import time

    cam=PiCamera()

    cam.resolution=resolution
    cam.exposure_mode=exposure_mode

    cam.start_preview()
    time.sleep(sleep)
    cam.stop_preview()


def take_picture(fname='outpic.jpg', resolution=(2592, 1944), framerate=15, brightness=50, 
        contrast=50, exposure_mode='auto', format=format, sleep=5, iso=None,
        shutter_speed=None):
    from picamera import PiCamera
    import time

    cam=PiCamera()

    cam.resolution=resolution
    cam.framerate=framerate
    cam.brightness=brightness
    cam.contrast=contrast
    cam.exposure_mode=exposure_mode

    if iso is not None:
        cam.iso=iso
    if shutter_speed is not None:
        cam.shutter_speed = shutter_speed

    cam.start_preview()
    time.sleep(sleep)
    cam.capture(fname, format=format)
    cam.stop_preview()
    cam.close()
