from cam_lib import take_picture, get_fname 
import fractions as frac

format='png'
fname=get_fname('pictures/pic_1280_auto', format)

take_picture(fname=fname, resolution=(1280, 720), format=format, exposure_mode='auto')
