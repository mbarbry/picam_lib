from cam_lib import take_picture, get_fname 
import fractions as frac

format='png'
exposure_mode = ['auto', 'night', 'nightpreview', 'backlight', 'sports', 'snow', 'verylong', 'fixedfps', 'antishake']


for exp in exposure_mode:
    fname=get_fname('pictures/pic_1280_'+exp, format)

    take_picture(fname=fname, resolution=(1280, 720), format=format, exposure_mode=exp)
