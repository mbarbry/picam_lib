from cam_lib import take_picture, get_fname 
import fractions as frac

format='jpeg'
fname=get_fname('test', format)

take_picture(fname=fname, framerate=frac.Fraction(1, 6), resolution=(3280, 2464), format=format, sleep=30, iso=800, 
        shutter_speed=6000000, exposure_mode='off')
