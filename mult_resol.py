from cam_lib import take_picture, get_fname 
import fractions as frac

format='png'
resolution=[(640, 480), (1280, 720), (1640, 1232), (1920, 1080), (3280, 2464)]

for res in resolution:
    fname=get_fname('pictures/pic_{0}_auto'.format(res[0]), format)

    take_picture(fname=fname, resolution=res, format=format, exposure_mode='auto')
