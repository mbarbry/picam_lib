from cam_lib import take_picture, get_fname 
import fractions as frac

format='png'
fname=get_fname('pictures/pic_640_night', format)

take_picture(fname=fname, resolution=(640, 640), format=format, exposure_mode='night')
