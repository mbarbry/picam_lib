A small python library to take pictures with the picamera.
The library just made use to the picamera library (https://picamera.readthedocs.io/en/latest/index.html).

The final goal is to write nice functions to take easily pictures using the pi with a telescope. 
And probably later also some routines to pilot the telescope with the pi.
The raspberry pi used for testing this library is a raspberry pi A+ with 
a camera v2. I noticed that to take pictures at high resolution the 
gpu memory must be increase to 256 MB.
