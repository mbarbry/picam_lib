from cam_lib import take_picture, get_fname
import fractions as frac

format="png"
fname=get_fname("pictures/pic_1280_long_expo", format)

take_picture(fname=fname, framerate=frac.Fraction(1, 6), resolution=(1280, 720), format=format, sleep=10, iso=800, 
        shutter_speed=6000000, exposure_mode='off')
