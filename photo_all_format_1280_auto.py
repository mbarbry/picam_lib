from cam_lib import take_picture, get_fname 
import fractions as frac

format=['png', 'jpeg', 'gif', 'bmp', 'rgba']

for f in format:
    fname=get_fname('pictures/pic_1280_auto', f)

    take_picture(fname=fname, resolution=(1280, 720), format=f, exposure_mode='auto', sleep=3)
